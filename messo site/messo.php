<!doctype html>
<html>
  <head>
    <title>Messo Official</title>
    <link rel="stylesheet" href="messo.css">
  </head>
  <body>
      <table class="maintable">
          <tr>
              <td>
                    <table class="header">
                        <tr>
                                        <td class="giflogo" ><img src="Image/DisastrousLiveIrrawaddydolphin-max-1mb.gif" height="60px"></td>
                                        <td class="logo"> LikeKart </td>
                                        <td class="heading"><a href="sellerlogin.php">Sell Online</a></td>
                                        <td class="heading"><a href="#"></a>How it Works</td>
                                        <td class="heading"><a href="#"></a>Pricing & Commision</td>
                                        <td class="heading"><a href="#"></a>Shipping & Returns</td>
                                        <td class="heading"><a href="#"></a>Grow Business</td>
                                        <td class="heading"><a href="userlogin.php"><button class="login">Login</button></a></td>
                                        <td class="heading"><a href="userform.php"><button class="start">Sign Up</button></a></td>
                        </tr>
                    </table>
                    <table class="table1">
                      <tr>
                        <td class="selli">
                          <b>Sell online to 10 Cr+ <br> customers at <br> <span class="comi"> 0% Commission </span> <br></b>
                          <span class="become"> Become a LikeKart seller and grow your business across India </span> <br> <br>
                          <div class="search-container">000
                          </div>

                        </td>
                        <td class="imageback">
                          <img src="image/photo.png">
                        </td>
                      </tr>
                    </table>
                    <table class="table2">
                      <tr align="center">
                          <td>
                            <table class="table21">
                              <tr>
                                <td>
                                  <table class="table11">
                                    <tr>
                                      <td> <span class="lakh"> <b> 4 lakh+ <br> <span class="trust"> Trust LikeKart to sell <br> &nbsp;&nbsp; online </b></span> </td>
                                    </tr>
                                  </table>
                                </td>
                                <td>
                                  <table class="table11">
                                    <tr>
                                      <td><span class="lakh"> <b> 10 Crore+ <br> <span class="trust"> Customers buying  <br> &nbsp;&nbsp; across India</b> </span></td>
                                    </tr>
                                  </table>
                                </td>
                                <td>
                                  <table class="table11">
                                    <tr>
                                      <td><span class="lakh"> <b> 270000+ <br> <span class="trust"> Pincode Supported  <br> &nbsp;&nbsp; for delivery</b> </span></td>
                                    </tr>
                                  </table>
                                </td>
                                <td>
                                  <table class="table11">
                                    <tr>
                                      <td><span class="lakh"> <b> 700+ <br> <span class="trust"> Categories to sell  <br> &nbsp;&nbsp; online</b> </span> </td>
                                    </tr>
                                  </table>
                                </td>
                                </td>
                              </tr>
                            </table>
                          </td>
                      </tr>
                    </table>
                    <table class="table3">
                      <tr>
                        <td>
                          <table class="table12">
                            <tr>
                              <td class="why"> <b>
                                 Why Suppliers Love LikeKart </b> <br> <br>
                                 <p class="All"> All the benefits that come with selling on LikeKart are <br> designed to help you sell more, and make it easier to grow <br> your business.</p>
                              </td>
                              <td>
                                <table class="table13">
                                  <tr>
                                    <td class="tin"><img src="Image/commision-removebg-preview.png"><b>0% Commission Fee</b><br> 
                                      <p class="sell">selling on LikeKart keep 100% of their profit by not paying <br> any commission</p>
                                    <span class="line"> _________________________________</span></td>
                                  </tr>
                                  <tr>
                                    <td class="tin"><img src="Image/see-removebg-preview.png"><b> 0 Penalty Charges</b> 
                                      <p class="sell">Sell online without the fear of order cancellation charges with 0 
                                      <br> Penalty for late dispatch or order cancellations.</p>
                                      <span class="line"> _________________________________</span></td>
                                  </tr>
                                  <tr>
                                    <td class="tin"><img src="Image/grow-removebg-preview.png"><b> Growth for Every Supplier </b> 
                                      <p class="sell">From small to large and unbranded to branded, all suppliers have <br> grown their businesses on LikeKart</p>
                                      <span class="line"> _________________________________</span></td>
                                  </tr>
                                  <tr>
                                    <td class="tin"><img src="Image/meet-removebg-preview.png"><b> Ease of Doing Business </b> 
                                      <p class="sell">Easy Product Listing &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lowest Cost Shipping <br>
                                        7-Day Payment Cycle from the delivery date</p>
                                        </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                    
                    <table class="table5">
                      <tr>
                        <td class="free">
                          <b>Experiences suppliers love to talk about</b>
                        </td>
                      </tr>
                    </table>
                    <table class="table6">
                      <tr>
                        <td>
                          <table class="table7">
                            <tr>
                              <td class="table2nd">
                                <img src="Image/rajat-amit-jain-testimonials.png" height="200px"> <br> <br> <h3> Amit and Rajat Jain </h3> <br> Smartees, Tiruppur <br> <br>
                                Our business has grown beyond <br> our imagination, getting upto <br> 10,000 orders consistently during <br> sale days. We are now <br>
                                constantly bringing new <br> products thanks to Meesho's insights.
                              </td>
                              <td class="table2nd">
                                <img src="Image/rathi-family-testimonial.png" height="200px"> <br> <br> <h3> Suman </h3> <br> Keshav Fashion, Hisar <br> <br>
                                I started selling on Meesho with <br> 4-5 orders on the very first day. <br> In no time I was getting over <br> 1000 orders a day, like a dream <br> come true.
                              </td>
                              <td class="table2nd">
                                <img src="Image/suman-testimonial.png" height="200px"> <br> <br> <h3> Mohit Rathi </h3> <br> Meira Jewellery, Ahmedabad <br> <br>
                                Meesho made it extremely simple to transition to online business during lockdown. Suddenly we were all over India to our surprise, seeing up to 5X growth on sale days.
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
            </td>
          </tr>
      </table>

  </body>
</html>