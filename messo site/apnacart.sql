-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 12, 2022 at 12:57 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apnacart`
--

-- --------------------------------------------------------

--
-- Table structure for table `admine`
--

CREATE TABLE `admine` (
  `id` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `phone` bigint(10) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(6) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admine`
--

INSERT INTO `admine` (`id`, `name`, `phone`, `email`, `dob`, `gender`, `password`) VALUES
(1, 'Rahul raj', 6200023941, 'arman786@gmail.com', '2022-09-27', '', '123');

-- --------------------------------------------------------

--
-- Table structure for table `adreess`
--

CREATE TABLE `adreess` (
  `id` int(11) NOT NULL,
  `village` varchar(20) DEFAULT NULL,
  `post` varchar(20) DEFAULT NULL,
  `dist` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `pincode` bigint(20) DEFAULT NULL,
  `u_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `c_id` int(11) NOT NULL,
  `p_id` int(11) DEFAULT NULL,
  `u_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`c_id`, `p_id`, `u_id`) VALUES
(77, 2, 1),
(82, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `o_id` int(11) NOT NULL,
  `order_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `payment_method` varchar(25) DEFAULT NULL,
  `u_id` int(11) DEFAULT NULL,
  `p_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`o_id`, `order_date`, `payment_method`, `u_id`, `p_id`) VALUES
(18, '2022-09-27 08:34:38', 'cash on de', 3, 1),
(19, '2022-09-27 08:34:39', 'cash on de', 3, 1),
(20, '2022-09-27 08:34:40', 'cash on de', 3, 1),
(21, '2022-09-27 08:34:41', 'cash on de', 3, 1),
(22, '2022-09-27 08:34:42', 'cash on de', 3, 1),
(23, '2022-09-27 08:34:42', 'cash on de', 3, 1),
(24, '2022-09-27 08:34:43', 'cash on de', 3, 1),
(25, '2022-09-27 08:34:43', 'cash on de', 3, 1),
(26, '2022-09-27 08:34:44', 'cash on de', 3, 1),
(27, '2022-09-27 08:34:44', 'cash on de', 3, 1),
(28, '2022-09-27 08:34:44', 'cash on de', 3, 1),
(29, '2022-09-27 08:34:45', 'cash on de', 3, 1),
(30, '2022-09-27 08:35:23', 'cash on de', 3, 1),
(31, '2022-09-27 08:35:24', 'cash on de', 3, 1),
(32, '2022-09-27 08:35:26', 'cash on de', 3, 1),
(33, '2022-09-27 08:35:27', 'cash on de', 3, 1),
(34, '2022-09-27 08:35:28', 'cash on de', 3, 1),
(35, '2022-09-27 08:35:29', 'cash on de', 3, 1),
(55, '2022-09-27 09:39:07', 'cash on delevery', 3, 3),
(56, '2022-09-27 09:40:02', 'cash on delevery', 3, 1),
(58, '2022-09-27 09:40:35', 'cash on delevery', 3, 1),
(59, '2022-09-27 09:41:07', 'cash on delevery', 3, 1),
(60, '2022-09-27 09:41:33', 'cash on delevery', 3, 3),
(61, '2022-09-27 09:41:45', 'cash on delevery', 3, 3),
(63, '2022-09-27 09:48:05', 'cash on delevery', 4, 2),
(64, '2022-09-27 09:48:31', 'cash on delevery', 4, 2),
(65, '2022-09-27 10:00:58', 'cash on delivery', 4, 2),
(66, '2022-09-27 10:02:10', 'cash on delivery', 4, 2),
(67, '2022-09-27 10:02:21', 'cash on delivery', 4, 2),
(68, '2022-09-27 10:03:55', 'cash on delivery', 4, 2),
(69, '2022-09-27 10:10:19', 'cash on delivery', 4, 2),
(72, '2022-09-28 07:16:29', 'cash on delivery', 1, 5),
(73, '2022-10-01 11:17:29', 'cash on delivery', 1, 5),
(74, '2022-10-08 08:56:54', 'cash on delivery', 1, 2),
(75, '2022-10-10 08:14:06', 'cash on delivery', 1, 2),
(78, '2022-10-10 08:58:05', 'cash on delivery', 1, 1),
(79, '2022-10-10 08:58:13', 'cash on delivery', 1, 1),
(80, '2022-10-10 09:02:40', 'cash on delivery', 1, 1),
(81, '2022-10-10 09:03:30', 'cash on delivery', 1, 1),
(82, '2022-10-10 09:34:21', 'cash on delivery', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `p_name` varchar(20) DEFAULT NULL,
  `price` bigint(10) DEFAULT NULL,
  `details` varchar(10) DEFAULT NULL,
  `color` varchar(10) DEFAULT NULL,
  `image` blob DEFAULT NULL,
  `s_id` int(11) DEFAULT NULL,
  `quantity` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `p_name`, `price`, `details`, `color`, `image`, `s_id`, `quantity`) VALUES
(1, 'phone', 123654, '4 gb ram', 'black', 0x75706c6f616465647069632f706f6c796e657369612d346b2d3139323078313038302e6a7067, 1, 100000),
(2, 'radmi', 10999, '4 gb ram ', 'blue', 0x2075706c6f616465647069632f70686f746f2d313531313730373137313633342d3566383937666630326161392e61766966, 1, 0),
(3, 'radmi note pro', 10999, '4 gb ram ', 'blue', 0x2075706c6f616465647069632f70686f746f2d313436383433363133393036322d6636306137316335633839322e61766966, 1, 0),
(4, 'Laptop', 31999, '8gb ', 'silver', 0x75706c6f616465647069632f70686f746f2d313436383433363133393036322d6636306137316335633839322e61766966, 2, 0),
(5, 'i phone', 109990, '8gb ', 'blue', 0x75706c6f616465647069632f626c75652d6974616c792d3139323078313038302e6a7067, 1, 28),
(6, 'i phone', 109990, '8gb ', 'blue', 0x2075706c6f616465647069632f70686f746f2d313531313730373137313633342d3566383937666630326161392e61766966, 1, 0),
(7, 'i phone', 109990, '8gb ', 'blue', 0x2075706c6f616465647069632f70686f746f2d313531313730373137313633342d3566383937666630326161392e61766966, 1, 0),
(8, 'i phone', 109990, '8gb ', 'blue', 0x2075706c6f616465647069632f70686f746f2d313531313730373137313633342d3566383937666630326161392e61766966, 1, 0),
(9, 'i phone', 109990, '8gb ', 'blue', 0x2075706c6f616465647069632f70686f746f2d313531313730373137313633342d3566383937666630326161392e61766966, 1, 0),
(11, 'phone', 123654, '4 gb ram', 'black', 0x2075706c6f616465647069632f70686f746f2d313531313730373137313633342d3566383937666630326161392e61766966, 1, 0),
(12, 'phone', 123654, '4 gb ram', 'black', 0x75706c6f616465647069632f70686f746f2d313436383433363133393036322d6636306137316335633839322e61766966, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `seller`
--

CREATE TABLE `seller` (
  `id` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `phone` bigint(10) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(6) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `s_status` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `seller`
--

INSERT INTO `seller` (`id`, `name`, `phone`, `email`, `dob`, `gender`, `password`, `s_status`) VALUES
(1, 'Arman kumar', 1234567890, 'abhi@gmail.com', '2013-10-02', 'male', '123456', 1),
(2, 'Rahul Raj', 8709579455, 'rahulray.raj@gmail.c', '2022-09-10', 'male', 'rahul@123', 1),
(3, 'ankit kumar', 7780084271, 'ankit@12gmail.com', '2003-01-29', 'male', '123456', 1),
(5, 'nitish kumar', 9999999999, 'nitish@gmail.com', '2011-01-05', 'male', '123456', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `phone` bigint(10) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `gender` varchar(6) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `u_status` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `phone`, `email`, `dob`, `gender`, `password`, `u_status`) VALUES
(1, 'anurag kumar', 8651810305, 'ankit@123gmail.com', '2022-09-08 00:00:00', 'male', '123456', 1),
(3, 'Arman', 6200023941, 'arman123@gmail.com', '2022-09-06 00:00:00', 'male', '123', 1),
(4, 'Guddu kumar', 7488665414, 'guddukumar.edugaon@g', '2022-09-28 00:00:00', 'male', '123', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admine`
--
ALTER TABLE `admine`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone` (`phone`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `adreess`
--
ALTER TABLE `adreess`
  ADD PRIMARY KEY (`id`),
  ADD KEY `u_id` (`u_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`c_id`),
  ADD KEY `p_id` (`p_id`),
  ADD KEY `u_id` (`u_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`o_id`),
  ADD KEY `p_id` (`p_id`),
  ADD KEY `u_id` (`u_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `s_id` (`s_id`);

--
-- Indexes for table `seller`
--
ALTER TABLE `seller`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone` (`phone`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone` (`phone`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admine`
--
ALTER TABLE `admine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `adreess`
--
ALTER TABLE `adreess`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `o_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `seller`
--
ALTER TABLE `seller`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `adreess`
--
ALTER TABLE `adreess`
  ADD CONSTRAINT `adreess_ibfk_1` FOREIGN KEY (`u_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `cart_ibfk_2` FOREIGN KEY (`u_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`u_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`s_id`) REFERENCES `seller` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
