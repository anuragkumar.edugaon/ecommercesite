<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <title>Hello, modularity!</title>
  </head>
  <body>
    <h1><?php session_start();
    $name= $_SESSION['p_name'];
     echo "HELLO $name"; ?></h1> <br><br>
   <a href="http://localhost/messo%20site/productdetail.php" ><button class="btn btn-primary btn-lg"  >PRODUCT DETAILS</button></a>
   <a href="http://localhost/messo%20site/addcartdetails.php" > <button  class="btn btn-primary btn-lg" >ADD CART DETAILS</button></a>
    <a href="http://localhost/messo%20site/orderdetails.php" ><button  class="btn btn-primary btn-lg">ORDER DETAILS</button></a>
   <a href="http://localhost/messo%20site/admin/sellerdetails.php" > <button  class="btn btn-primary btn-lg" >SELLER DETAILS</button></a>
   <a href="http://localhost/messo%20site/admin/userdetails.php" > <button   class="btn btn-primary btn-lg" >USER DETEILS</button></a>
   <br><br>
   <a href="http://localhost/messo%20site/admin/adminlogout.php" ><button class="btn btn-primary btn-lg" >LOGOUT</button>
    <script async src="https://cdn.jsdelivr.net/npm/es-module-shims@1/dist/es-module-shims.min.js" crossorigin="anonymous"></script>
    <script type="importmap">
    {
      "imports": {
        "@popperjs/core": "https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js",
        "bootstrap": "https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.esm.min.js"
      }
    }
    </script>
    <script type="module">
      import * as bootstrap from 'bootstrap'

      new bootstrap.Popover(document.getElementById('popoverButton'))
    </script>
  </body>
</html>